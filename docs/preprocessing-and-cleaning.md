# 데이터 전처리와 클리닝 기법

## <a name="intro"></a> 개요
어떤 데이터 분석이나 기계 학습 프로젝트에서도 데이터 전처리와 클리닝은 필수적인 단계이다. 추가적인 처리와 모델링에 적합하도록 원시 데이터를 변환하고 표준화하고 품질을 향상시키는 것을 포함한다.

이 포스팅에서는 Python을 사용하여 기계 학습 모델의 데이터를 전처리하고 클리닝하는 방법을 설명한다.

- 결측치, 이상치, 중복, 불일치, 오류 등과 같은 데이터 품질 문제와 과제를 파악하고 처리한다.
- panda, numpy, scikit-learn 등의 데이터 클리닝 방법과 도구를 적용하여 데이터를 조작, 필터링 및 수정한다.
- 스케일링, 정규화, 인코딩 및 비닝과 같은 데이터 변환과 표준화를 수행하여 데이터를 호환시키고 비교할 수 있도록 한다.
- 상관관계 분석, 주성분 분석, 카이제곱 검정과 같은 피처 선택과 추출 기법을 사용하여 데이터의 차원과 복잡성을 줄인다.
- 평균, 중앙값, 최빈값, k-최근접 이웃(k-nearest neighbors), 격리 포리스트 등 통계적 방법과 기계 학습 방법을 사용하여 이상치를 대입하고 탐지한다.
- matplotlib, seaborn, plotly 등을 사용하여 데이터를 시각화하고 탐색하여 데이터에 대한 통찰력을 얻고 패턴과 추세를 파악한다.

데이터 전처리와 청소의 세계로 뛰어들 준비가 되었나요? 시작해 봅시다!

## <a name="sec_02"></a> 데이터 품질 문제와 과제
데이터 품질은 데이터가 데이터 분석이나 기계 학습 작업의 요구사항과 기대사항을 얼마나 잘 충족하는지를 측정하는 것이다. 데이터 품질은 데이터의 출처, 수집, 저장, 처리, 전송 등 다양한 요인에 의해 영향을 받을 수 있다. 데이터 품질이 좋지 않을 경우 기계 학습 모델의 성능과 효율성 저하는 물론 부정확하고 신뢰할 수 없으며 오해의 소지가 있는 결과와 결론을 초래할 수 있다.

데이터 분석 또는 머신 러닝 프로젝트에서 직면할 수 있는 일반적인 데이터 품질 문제와 과제는 다음과 같다.

- **결측치(missing value)**: 결측치는 데이터세트의 일부 데이터 포인트 또는 속성이 기록되지 않았거나나 이용 가능하지 않을 때 발생한다. 결측치는 인적 오류, 시스템 고장 또는 의도적인 누락 등 다양한 원인에 의해 발생할 수 있다. 결측치는 표본 크기를 줄이고 편향이 나타날 수 있으며 데이터의 분포와 상관 관계에 영향을 미치기 때문에 통계 분석과 기계 학습 모델에 영향을 미칠 수 있다.
- **이상치(outlier)**: 이상치는 데이터의 나머지들에서 크게 벗어나는 데이터 포인트 또는 속성이다. 이상치는 측정 오류, 자연 변동성 또는 어노멀리 같은 다양한 이유로 인해 발생할 수 있다. 이상치는 데이터의 평균, 표준 편차 및 범위뿐만 아니라 데이터 간의 관계와 패턴을 왜곡하므로 통계 분석과 기계 학습 모델에 영향을 미칠 수 있다.
- **중복(duplicate)**: 중복은 데이터세트에서 반복되거나 복사된 데이터 포인트 또는 속성이다. 중복은 인적 오류, 시스템 장애 또는 데이터 통합과 같은 다양한 이유로 인해 발생할 수 있다. 중복은 표본 크기를 증가시키고 편향을 도입하며 데이터의 고유성과 일관성에 영향을 미치기 때문에 통계 분석과 기계 학습 모델에 영향을 미칠 수 있다.
- **비일관성(inconsitency)**: 비일관성은 데이터의 나머지 부분과 일치 또는 조화를 이루지 않는 데이터 포인트 또는 속성이다. 비일관성은 인적 오류, 시스템 장애 또는 데이터 통합 등 다양한 이유로 인해 발생할 수 있다. 비일관성은 데이터의 정확성, 신뢰성과 유효성을 떨어뜨리기 때문에 통계 분석과 기계 학습 모델에 영향을 미칠 수 있다.
- **오류(error)**: 오류는 데이터세트에서 부정확한 데이터 포인트 또는 속성이다. 오류는 인적 오류, 시스템 장애 또는 데이터 통합 등 다양한 이유로 인해 발생할 수 있다. 오류는 데이터의 정확성, 신뢰성 및 유효성을 떨어뜨리기 때문에 통계 분석 및 기계 학습 모델에 영향을 미칠 수 있다.

이러한 데이터 품질 문제와 과제를 어떻게 파악하고 처리할 수 있을까? 다음 절에서는 데이터 클리닝 방법과 도구를 사용하여 데이터를 조작하고 필터링하고 수정하는 방법을 살펴볼 것이다.

## <a name="sec_03"></a> 데이터 클리닝 방법과 도구
데이터 클리닝은 앞 절에서 논의한 데이터 품질 문제와 과제를 감지하고 수정(또는 제거)하는 과정이다. 데이터 클리닝은 기계 학습 모델의 성능과 정확성뿐만 아니라 데이터의 품질과 사용성을 향상시킬 수 있다.

이 절에서는 Python에서 가장 대중적이고 강력한 데이터 클리닝 방법과 도구를 사용하는 방법을 배울 것이다:

- 데이터 분석과 조작을 위한 Python 라이브러리인 `panda`를 활용하여 데이터를 읽고 쓰고 탐색한다.
- 데이터에 대한 수치 연산과 계산을 수행하려면 과학 컴퓨팅을 위한 Python 라이브러리인 `numpy`를 사용한다.
- 기계 학습을 위한 Python 라이브러리인 `scikit-learn`을 사용하여 스케일링, 인코딩, 대치, 이상치 탐지 등의 데이터 전처리와 클리닝 기법을 적용한다.

```python
# Import the libraries
import pandas as pd
import numpy as np
from sklearn import preprocessing


# Create a sample DataFrame
data = {'Name': ['John', 'Anna', 'Peter', 'Linda', 'Jack'],
        'Age': [28, 35, 45, np.nan, 52],
        'Salary': [30000, 40000, np.nan, 50000, 60000],
        'Gender': ['M', 'F', 'M', 'F', 'M']}
df = pd.DataFrame(data)

# Check for missing values
print("Missing values before cleaning:")
print(df.isnull().sum())

# Fill missing values
df['Age'].fillna(df['Age'].mean(), inplace=True)
df['Salary'].fillna(df['Salary'].median(), inplace=True)

# Encode categorical variables
le = preprocessing.LabelEncoder()
df['Gender'] = le.fit_transform(df['Gender'])

# Normalize numerical variables
scaler = preprocessing.MinMaxScaler()
df[['Age', 'Salary']] = scaler.fit_transform(df[['Age', 'Salary']])

# Display the cleaned DataFrame
print("\nCleaned DataFrame:")
print(df)
```

## <a name="sec_04"></a> 데이터 변환과 
데이터 변환과 표준화는 데이터의 형식, 구조, 규모 등을 변경하여 추후 분석과 모델링에 호환과 비교가 가능하도록 하는 과정이다. 데이터 변환과 표준화는 기계 학습 모델의 성능과 정확성을 향상시킬 수 있을 뿐만 아니라 결과의 해석 가능성과 시각화를 향상시킬 수 있다.

이 절에서는 Python에서 가장 일반적이고 유용한 데이터 변환과 표준화 기술을 사용하는 방법을 보인다.

- 최소-최대 스케일링(min-max scaling), 표준 스케일링(standard scaling) 및 강력한 스케일링(robust scaling)과 같은 방법을 사용하여 데이터를 0 대 1 또는 -1 대 1과 같은 공통 범위로 스케일링한다.
- l1 정규화, l2 정규화 및 최대 정규화 같은 방법을 사용하여 데이터를 L1 norm와 lL norm같은 단위 norm으로  정규화한다.
- 레이블 인코딩, 순서 인코딩(ordinal) 및 원 핫(on-hot) 인코딩 같은 방법을 사용하여 이진, 순서 인코딩 또는 원 핫 인코딩 같은 수치 값으로 범주형 데이터를 인코딩한다.
- cut, qcut 및 kbins 이산화기와 같은 방법을 사용하여 수치 데이터를 등폭(equal-widdth), 등빈도(equal-frequency) 또는 k-mean 비닝(binning) 같은 이산 간격으로 비닝한다.

```python
# Import the preprocessing module
from sklearn import preprocessing

# Sample data
data = [[-1, 2], [-0.5, 6], [0, 10], [1, 18]]

# Standardization
scaler = preprocessing.StandardScaler().fit(data)
data_standardized = scaler.transform(data)

# Normalization
scaler = preprocessing.MinMaxScaler().fit(data)
data_normalized = scaler.transform(data)

# Binarization
binarizer = preprocessing.Binarizer(threshold=0.0).fit(data)
data_binarized = binarizer.transform(data)

# Print the results
print("Standardized Data:")
print(data_standardized)
print("\nNormalized Data:")
print(data_normalized)
print("\nBinarized Data:")
print(data_binarized)
```

## <a name="sec_05"></a> 데이터 축소와 피처 선택
데이터 축소와 특징 선택은 데이터 분석이나 기계 학습 작업에 가장 관련성이 높고 유익한 피처나 속성을 선택하여 데이터의 차원성과 복잡성을 줄이는 과정이다. 데이터 축소와 피처 선택은 기계 학습 모델의 성능과 정확성은 물론 결과의 해석 가능성과 시각화를 향상시킬 수 있다.

이 절에서는 Python에서 가장 일반적이고 효과적인 데이터 축소와 피처 선택 기법을 보인다.

- 상관관계 분석을 사용하여 두 피처 또는 속성 간의 선형 관계를 측정하고 상관관계가 높거나 중복된 특징을 제거한다.
- 주성분 분석(PCA)을 사용하여 원래의 피처 또는 속성을 데이터의 가장 큰 분산을 포착하는 상관없는 구성 요소들의 저차원 공간으로 변환한다.
- 카이-제곱 검정을 사용하여 범주형 피쳐 또는 속성과 범주형 타겟 또는 결과 사이의 의존성을 측정하고 카이-제곱 값이 가장 높은 피쳐를 선택한다.

```python
# Import the modules
from sklearn.feature_selection import chi2
from sklearn.decomposition import PCA
from sklearn.datasets import load_iris

# Load the iris dataset
iris = load_iris()
X = iris.data
y = iris.target

# Perform feature selection using chi-square test
chi2_selector = chi2(X, y)
selected_features = chi2_selector[0]

# Perform data reduction using PCA
pca = PCA(n_components=2)
X_reduced = pca.fit_transform(X)

# Print the results
print("Selected Features (Chi-Square Test):")
print(selected_features)
print("\nReduced Data (PCA):")
print(X_reduced)
```

## <a name="sec_06"></a> 결측치 처리와 이상치 탐지
결측치 처리와 이상치 탐지는 [데이터 품질 문제와 과제](#sec_02) 절에서 논의한 결측치와 이상치를 처리하는 과정이다. 결측치 처리와 이상치 탐지는 기계 학습 모델의 성능과 정확성뿐만 아니라 데이터의 품질과 사용성을 향상시킬 수 있다.

이 절에서는 Python에서 가장 일반적이고 효과적인 결측치 처리와 이상치 탐지 기술을 사용하는 방법을 보인다.

- 평균, 중위수, 모드 또는 상수 값을 사용하여 데이터의 결측 값을 채우려면 단순 임퓨터, 채우기 또는 교체와 같은 방법을 사용한다.
- knn imputer 또는 fancyimpute 같은 방법을 사용하여 가장 가까운 이웃의 유사성을 기반으로 데이터의 결측값을 추정하는 KNN을 사용한다.
- z-score, IQR(사분위간 범위) 또는 격리 포리스트(isolation forest) 같은 방법을 사용하여 데이터의 이상치를 식별하고 제거한다.

```python
# Import the modules
from sklearn.impute import SimpleImputer, KNNImputer
from sklearn.ensemble import IsolationForest
from fancyimpute import KNN
import numpy as np
import pandas as pd

# Sample data
data = {'A': [1, 2, np.nan, 4, 5],
        'B': [10, 20, 30, np.nan, 50],
        'C': [100, np.nan, 300, 400, 500]}

df = pd.DataFrame(data)

# Impute missing values using mean, median, mode, or constant
simple_imputer = SimpleImputer(strategy='mean')
df_imputed = pd.DataFrame(simple_imputer.fit_transform(df), columns=df.columns)

# Impute missing values using KNN
knn_imputer = KNNImputer(n_neighbors=2)
df_knn_imputed = pd.DataFrame(knn_imputer.fit_transform(df), columns=df.columns)

# Impute missing values using fancyimpute's KNN
df_fancy_knn_imputed = pd.DataFrame(KNN(k=3).fit_transform(df), columns=df.columns)

# Detect outliers using Isolation Forest
isolation_forest = IsolationForest(contamination=0.1)
outliers = isolation_forest.fit_predict(df_imputed)

# Remove outliers
df_no_outliers = df_imputed[outliers != -1]

# Print the results
print("Original Data:")
print(df)
print("\nImputed Data (Simple Imputer - Mean):")
print(df_imputed)
print("\nImputed Data (KNN Imputer):")
print(df_knn_imputed)
print("\nImputed Data (Fancyimpute KNN):")
print(df_fancy_knn_imputed)
print("\nOutliers Detected (Isolation Forest):")
print(outliers)
print("\nData without Outliers:")
print(df_no_outliers)
```

## <a name="sec_07"></a> 데이터 시각화와 탐색
데이터 시각화와 탐색은 차트, 플롯, 지도, 대시보드 등 그래픽 도구와 상호 작용 도구를 사용하여 데이터를 제시하고 조사하는 과정이다. 데이터 시각화와 탐색은 데이터에 대한 통찰력을 얻고 패턴과 경향을 파악하는 데 도움이 될 수 있을 뿐만 아니라 자신의 발견과 결론을 다른 사람들과 소통하고 공유할 수 있다.

이 절에서는 Python에서 가장 인기 있고 강력한 데이터 시각화와 탐색 도구를 사용하는 방법을 보인다.

- 정적, 애니메이션된 및 대화형 시각화를 만들기 위한 Python 라이브러리인 `mathplotlib`를 사용하여 선, 막대, 파이, 산포도, 히스토그램 및 상자 그림 같은 다양한 타입의 차트를 사용하여 데이터를 플롯한다.
- 통계 데이터 시각화를 위한 Python 라이브러리인 `seaborn`을 사용하여 다양한 스타일, 테마, 팔레트 및 그리드를 사용하여 시각화를 향상시키고 맞춤화할 수 있으며 히트맵, 바이올린, 스웜, 페어 및 조인트 플롯과 같은 다양한 타입의 차트를 사용하여 데이터를 플롯할 수 있다.
- 대화형 및 고성능 데이터 시각화를 위한 Python 라이브러리인 `plotly`를 사용하여 선, 막대, 파이, 산포도, 히스토그램, 상자 그림 및 지도 같은 다양한 타입의 차트를 사용하여 대화형 또는 동적 시각화를 생성하고 디스플레이한다.

```python
# Install the libraries using pip
pip install matplotlib
pip install seaborn
pip install plotly

# Import the libraries
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import pandas as pd
import numpy as np

# Sample data
np.random.seed(0)
data = pd.DataFrame({
    'Category': ['A', 'B', 'C', 'D', 'E'],
    'Value': np.random.randint(1, 100, 5),
    'Value2': np.random.randint(1, 100, 5)
})

# Matplotlib - Line chart
plt.figure(figsize=(8, 4))
plt.plot(data['Category'], data['Value'])
plt.title('Line Chart')
plt.xlabel('Category')
plt.ylabel('Value')
plt.show()

# Matplotlib - Bar chart
plt.figure(figsize=(8, 4))
plt.bar(data['Category'], data['Value'])
plt.title('Bar Chart')
plt.xlabel('Category')
plt.ylabel('Value')
plt.show()

# Seaborn - Scatter plot
plt.figure(figsize=(8, 4))
sns.scatterplot(x='Value', y='Value2', data=data)
plt.title('Scatter Plot')
plt.xlabel('Value')
plt.ylabel('Value2')
plt.show()

# Seaborn - Heatmap
plt.figure(figsize=(8, 4))
sns.heatmap(data.pivot('Category', 'Value', 'Value2'), cmap='YlGnBu', annot=True)
plt.title('Heatmap')
plt.show()

# Plotly - Line chart
fig = px.line(data, x='Category', y='Value', title='Line Chart')
fig.show()

# Plotly - Bar chart
fig = px.bar(data, x='Category', y='Value', title='Bar Chart')
fig.show()

# Plotly - Pie chart
fig = px.pie(data, names='Category', values='Value', title='Pie Chart')
fig.show()
```

## <a name="summary"></a> 마치며
이 포스팅에서는 Python을 사용하여 기계 학습 모델의 데이터를 전처리하고 정리하는 방법을 설명하였다.

- 결측치, 이상치, 중복, 불일치, 오류 등 데이터 품질 문제와 과제를 파악하고 처리한다.
- panda, numpy, scikit-learn 등의 데이터 클리닝 방법과 도구를 적용하여 데이터를 조작, 필터링 및 수정한다.
- 스케일링, 정규화, 인코딩 및 비닝과 같은 데이터 변환과 표준화를 수행하여 데이터를 호환될 수 있도록 그리고 비교할 수 있도록 한다.
- 상관관계 분석, 주성분 분석, 카이제곱 검정과 같은 피처 선택과 추출 기법을 사용하여 데이터의 차원과 복잡성을 줄인다.
- 평균, 중앙값, 최빈값, k-최근접 이웃, 격리 포리스트 같은 통계적 방법과 기계 학습 방법을 사용하여 결측치를 처리하고 이상치를 탐지한다.
- matplotlib, seaborn과 plotly를 사용하여 데이터를 시각화하고 탐색하여 통찰을 얻어 패턴과 추세를 파악한다.
