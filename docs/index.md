# 데이터 전처리와 클리닝 기법 <sup>[1](#footnote_1)</sup>

> <font size="3">기계 학습 모델을 위한 데이터 전처리와 클리닝 방법에 대해 알아본다.</font>

## 목차

1. [개요](./preprocessing-and-cleaning.md#intro)
1. [데이터 품질 문제와 과제](./preprocessing-and-cleaning.md#sec_02)
1. [데이터 클리닝 방법과 도구](./preprocessing-and-cleaning.md#sec_03)
1. [데이터 변환과 표준화](./preprocessing-and-cleaning.md#sec_04)
1. [데이터 축소와 피처 선택](./preprocessing-and-cleaning.md#sec_05)
1. [결측치 처리와 이상치 탐지](./preprocessing-and-cleaning.md#sec_06)
1. [데이터 시각화와 탐색](./preprocessing-and-cleaning.md#sec_07)
1. [마치며](./preprocessing-and-cleaning.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 23 — Data Preprocessing and Cleaning Techniques](https://levelup.gitconnected.com/ml-tutorial-23-data-preprocessing-and-cleaning-techniques-0f4da5b3a3dc?sk=be02688df736f42683890f9a7a71a61f)를 편역하였습니다.
